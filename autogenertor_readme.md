# 关键词查看
        `autogenertor -h`

# YAML关键词解释
## config中的关键词
        `
        - author: 测试者
          cc:
          - 抄送者1的邮箱
          - 抄送者2的邮箱
          description: 测试报告的备注信息
          disable_image: false（禁止加载图片，true或者false）
          disable_js: false（禁止运行JS，true或者false）
          disable_plugin: false（禁止加载插件，true或者false）
          driverPath: chromedriver.exe的目录（默认为null，可以不写）
          email: 发送者QQ邮箱地址
          headless: false（启用无头-浏览器无界面运行，true或者false）
          hide_scrollbars: false（禁止浏览器滚动条，true或者false）
          isTestCase: true
          is_dev: false（启用浏览器开发者模式，true或者false）
          is_max: true（浏览器最大化，true或者false）
          is_min: false（浏览器最小化，true或者false）
          password: QQ邮箱授权码
          report_file_name: 测试报告名称
          smtp_port: 465（QQ邮箱SMTP SSL的端口号）
          smtpserver: smtp.qq.com
          title: 测试报告的标题
          to:
          - 邮件接收者地址1
          - 邮件接收者地址2                                                                              
        `