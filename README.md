# xiaobaidoc

#### 方法的介绍

    class Api(builtins.object)
            Methods defined here:
        api(self, method='GET', url='', assertText=None, **kwargs)
            接口请求，返回响应对象
            :param method:
            :param url:
            :param assertJson:      预留
            :param assertText:      包含
            :param kwargs:
            :return:
        dump(self, steam, *args, **kwargs)
            存储接口脚本信息
            :param steam:
            :param args:
            :param kwargs:
            :return:
        load(self, steam, *args, **kwargs)
            加载多个接口信息
            :param steam:
            :param args:
            :param kwargs:
            :return:

    class EmailHandler(builtins.object)
        __init__(self, smtpserver='smtp.xiaobaiit.com', port=25, sender_name='xxx@xx.com', sender_passwd='QQ邮箱授权码')
            初始化SMTP服务器
            :param smtp:            SMTP服务器
            :param port:            25
            :param sender_name:     邮箱账户
            :param sender_passwd:   邮箱密码
        sendemail(self, _to=None, _cc=None, title='自动化测试结果', email_content='', _type='plain', filename=None)
            发送信息
            :param _to:             接收人     ['a@qq.com','b@qq.com']
            :param _cc:             抄送人     ['c@qq.com','d@qq.com']
            :param title:           邮件标题
            :param email_content:   邮件正文
            :param _type:           邮件类型：plain、html
            :param filename:        附件
            :return:
            _to:             接收人     ['a@qq.com','b@qq.com']
            _cc:             抄送人     ['c@qq.com','d@qq.com']
            title:           邮件标题   ''
            email_content:   邮件正文   ''
            _type:           邮件类型：  plain、html
            filename:        附件         ['xiaobai.html']

    class pageObject(builtins.object)
        add_cookie(self, cookie_dict=None, cookie_str=None, *args, **kwargs)
            添加cookie
            :param cookie_dict:
            :param cookie_str:
            :param args:
            :param kwargs:
            :return:
            
            Adds a cookie to your current session.
            
            :Args:
            - cookie_dict: A dictionary object, with required keys - "name" and "value";
                optional keys - "path", "domain", "secure", "expiry"
            - cookie_str: A string object, with required keys - "name" and "value";
                optional keys - "path", "domain", "secure", "expiry"
            
            Usage:
                driver.add_cookie(cookie_dict={'name' : 'foo', 'value' : 'bar'})
                driver.add_cookie((cookie_dict={'name' : 'foo', 'value' : 'bar', 'path' : '/'})
                driver.add_cookie((cookie_dict={'name' : 'foo', 'value' : 'bar', 'path' : '/', 'secure':True})
                driver.add_cookie((cookie_str='foo=bar; check=true')
        alertNo(self, *args, **kwargs)
            点击JS弹出框取消按钮
            :return:
        alertSend(self, data, *args, **kwargs)
            JS弹出框输入内容
            :param data:    输入的内容
            :return:
        alertYes(self, *args, **kwargs)
            点击JS弹出框确认按钮
            :return:
        assertContent(self, loc, attr='', *args, **kwargs)
            判断元素是否存在或者属性是否存在
            :param loc:
            :param attr:
            :param args:
            :param kwargs:
            :return:
        back(self)
            浏览器撤销后退
            :return:
        close(self)
            关闭界面
            :return:
        cur_url(self)
            获取当前的url
            :return:
        delCookie(self, name=None, is_all=False)
            删除当前站点cookie
            :param name:
            :param is_all:
            :return:
            name与is_all只能二选一，使用其中一个参数即可
        element_save_img(self, loc, filename)
            为当前元素截图
            :param loc:     当前元素xpath表达式
            :param filename 准备保存文件名
            :return:
            依据提供的元素对象，针对该元素进行截图并保存
        forward(self)
            前进
            :return:
        get(self, url, *args, **kwargs)
            打开网页
            :param url:
            :param args:
            :param kwargs:
            :return:
        getCookie(self, name=None, is_all=False)
            获取当前站点cookie
            :param name:
            :param is_all:
            :return:
            name与is_all只能二选一，使用其中一个参数即可
        init(self, driverPath=None, is_headless=False, is_max=False, is_min=False, wait=30, *args, **kwargs)
            初始化浏览器对象
            :param driverPath:  浏览器驱动
            :param is_headless: 是否无界面
            :param is_max:      是否最大化
            :param is_min:      是否最小化
            :param wait:        隐式等待时间（单位：秒）
            :param args:
            :param kwargs:
            :return:
            返回浏览器对象
            driverPath为浏览器驱动的路径，默认为系统路径下自行查找；
            is_headeless为浏览器设置无头模式，布尔类型，默认为False；
            is_max为设置浏览器最大化，布尔类型，默认为False;
            is_min为设置浏览器最小化，布尔类型，默认为False;
            wait为设置页面定位的隐式等待时间，整数型，默认30秒;
        move(self, src_loc, tag_loc, *args, **kwargs)
            从源元素位置拖动到目标元素位置
            :param src_loc:     源元素xpath表达式
            :param tag_loc:     目标元素xpath表达式
            :return:
        move_and_stop(self, loc, *args, **kwargs)
            悬停到某元素上
            :param loc:     目标元素xpath表达式
            :return:
        page_save_img(self, filename)
            将当前页面进行截图
            :param filename:
            :return:
        page_source(self)
            获取当前页面源码
            :return:
        quit(self)
            关闭浏览器
            :return:
        refresh(self)
            刷新页面
            :return:
        run_js(self, script)
            执行javascript脚本
            :param script:
            :return:
        switchFrame(self, loc, index=0, *args, **kwargs)
            切换内嵌页
            :param index:       内嵌页的索引值
            :param args:
            :param kwargs:
            :return:
        switchFrame2Window(self, *args, **kwargs)
            返回默认页
            :return:
        switchWindow(self, index=1, *args, **kwargs)
            切换标签页
            :param index:       标签页的索引值
            :param args:
            :param kwargs:
            :return:
            切换至第index标签页
        title(self)
            获取标题
            :return:
        xpath(self, loc, timeout=5, step=0.5, *args, **kwargs)
            定位元素
            :param loc:             xpath表达式
            :param timeout:         定位等待时间
            :param step:            检测的时间
            :param args:
            :param kwargs:
            :return:
            loc为定位元素的xpath定位表达式
            timeout为定位元素时的显式等待时间，整数型，默认5秒
            step为显示等待的间隔检测时间，浮点型，默认0.5秒
        xpaths(self, loc, index=None, *args, **kwargs)
            定位多个元素
            :param loc:             xpath表达式
            :param index            索引值
            :param args:
            :param kwargs:
            :return:
            loc为定位元素的xpath定位表达式
            index为需要返回所定位的元素中第index个元素，整数型，默认为空，返回多个元素对象