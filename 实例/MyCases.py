import unittest
# xiaobaiauto导入失败时，请将xiaobeauto包中的两个文件复制到自己的项目包中
from xiaobaiauto.auto import pageObject, Report, log, EmailHandler, Api, Keys

class MyTestCase(unittest.TestCase):
    def setUp(self):
        """
        初始化日志
        :return:
        """
        self.logger = log()
        self.client = Api()
        self.page = pageObject()
        self.driver = self.page.init(is_max=True)

    def test_web_12306(self):
        # 通过self.driver 调用原生方法
        # 通过self.page   调用集成方法
        self.page.get(url='https://kyfw.12306.cn/otn/leftTicket/init?linktypeid=dc&fs=%E4%B8%8A%E6%B5%B7,SHH&ts=%E9%83%91%E5%B7%9E,ZZF&date=2020-02-02&flag=N,N,Y')

        #self.page.add_cookie(cookie_dict={'name': '', 'value': ''})

        chufa = self.page.xpath('//*[@id="fromStationText"]')
        chufa.clear()
        chufa.send_keys('上海')
        chufa.send_keys(Keys.ENTER)

    def tearDown(self):
        pass

if __name__ == '__main__':
    report_file_name = 'testReport.html'
    suite = unittest.TestSuite()
    # 添加需要执行的测试用例
    suite.addTest(MyTestCase('test_web_12306'))
    # suite.addTest(MyTestCase('test_api_xxx'))  # 不运行就注释掉
    fp = open(report_file_name, 'wb')
    # 生成报告
    runner = Report(
        stream=fp,
        title='测试',
        description='备注信息',
        tester='Tser'
    )
    runner.run(suite)
    fp.close()
    # 将测试报告发送指定邮件<数据务必修改>建议使用QQ邮箱（port参数默认使用SSL端口）
    email = EmailHandler(smtp='smtp.qq.com', port=25, sender_name='qq号', sender_passwd='邮箱授权码')
    email.sendemail(
        _to=['1@qq.com', '2@qq.com'],
        _cc=['admin@163.com', 'leader@gmail.com'],
        title='邮件标题',
        email_content='邮箱内容',
        _type='html',
        filename=[report_file_name]
    )